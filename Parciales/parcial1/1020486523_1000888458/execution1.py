from parcial1 import RKG , RKA
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

if __name__=="__main__":
    """al ejecutar este codigo aparecen 3 graficas, la primera correspondiente a la edo y'=x-y, 
    que por el modo como de definio en RGK se imprime por defecto, por favor cerrar este ventana, 
    luego el codigo carga unos segundos para entregar las soluciones correspondiente a las condiciones 2)
    , por favor cerrar esta ventana para poder ver las graficas correspondientes a las soluciones
    bajo las condiciones 4), no se muestran las soluciones para 1) y 3) porque bajo las condiciones 
    de estos incisos, no hay dinamica del sistema.
    """
    print('Por favor cerrar primer ventana para poder ver las graficas correspondiente al sistema lorenz')
    #condiciones 2)
    def lorenz(t, y,sigma=10,rho=28,beta=8/3):  #Ecuaciones diferenciales
        dy= ([sigma * (y[1] - y[0]),
              y[0] * (rho - y[2]) - y[1],
              y[0] * y[1] - beta * y[2]])
        return np.array(dy)
    

    T=100
    y0=np.array([0.0,0.01,0.0])
    dt=0.01
    c_s=np.array([0,1/2,1/2,1])
    b_s=np.array([1/6,1/3,1/3,1/6])
    A= np.array([[0,0,0,0],[1/2,0,0,0],[0,1/2,0,0],[0,0,1,0]])

    sols=RKA(dt,lorenz,y0,0,T,A,c_s,b_s,4)
    puntos=sols.soluciones()
    #print(len(puntos[0,:]))
    """ax=plt.figure().add_subplot(projection='3d')
    ax.plot(puntos[0,:],puntos[1,:],puntos[2,:],'b')
    plt.show()"""

    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(2, 2, 1, projection='3d')

    # Trazar el gráfico 3D
    ax.plot(puntos[0, :], puntos[1, :], puntos[2, :], 'b')
    ax.set_title('Gráfico 3D')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    # Crear las subparcelas para los gráficos 2D
    ax2 = fig.add_subplot(2, 2, 2)
    ax3 = fig.add_subplot(2, 2, 3)
    ax4 = fig.add_subplot(2, 2, 4)

    # Trazar los gráficos 2D
    ax2.plot(puntos[0, :], puntos[1, :], 'r')
    ax2.set_title('Gráfico 2D-1')
    ax2.set_xlabel('X')
    ax2.set_ylabel('Y')

    ax3.plot(puntos[0, :], puntos[2, :], 'g')
    ax3.set_title('Gráfico 2D-2')
    ax3.set_xlabel('X')
    ax3.set_ylabel('Z')

    ax4.plot(puntos[1, :], puntos[2, :], 'y')
    ax4.set_title('Gráfico 2D-3')
    ax4.set_xlabel('Y')
    ax4.set_ylabel('Z')

    plt.tight_layout()
    plt.show()





    #condiciones 4)
    def lorenz(t, y,sigma=16,rho=45,beta=4):  #Ecuaciones diferenciales
        dy= ([sigma * (y[1] - y[0]),
              y[0] * (rho - y[2]) - y[1],
              y[0] * y[1] - beta * y[2]])
        return np.array(dy)
    

    T=100
    y0=np.array([0.0,0.001,0.0])
    dt=0.01
    c_s=np.array([0,1/2,1/2,1])
    b_s=np.array([1/6,1/3,1/3,1/6])
    A= np.array([[0,0,0,0],[1/2,0,0,0],[0,1/2,0,0],[0,0,1,0]])

    sols=RKA(dt,lorenz,y0,0,T,A,c_s,b_s,4)
    puntos=sols.soluciones()
    #print(len(puntos[0,:]))
    """ax=plt.figure().add_subplot(projection='3d')
    ax.plot(puntos[0,:],puntos[1,:],puntos[2,:],'b')
    plt.show()"""

    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(2, 2, 1, projection='3d')

    # Trazar el gráfico 3D
    ax.plot(puntos[0, :], puntos[1, :], puntos[2, :], 'b')
    ax.set_title('Gráfico 3D')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    # Crear las subparcelas para los gráficos 2D
    ax2 = fig.add_subplot(2, 2, 2)
    ax3 = fig.add_subplot(2, 2, 3)
    ax4 = fig.add_subplot(2, 2, 4)

    # Trazar los gráficos 2D
    ax2.plot(puntos[0, :], puntos[1, :], 'r')
    ax2.set_title('Gráfico 2D-1')
    ax2.set_xlabel('X')
    ax2.set_ylabel('Y')

    ax3.plot(puntos[0, :], puntos[2, :], 'g')
    ax3.set_title('Gráfico 2D-2')
    ax3.set_xlabel('X')
    ax3.set_ylabel('Z')

    ax4.plot(puntos[1, :], puntos[2, :], 'y')
    ax4.set_title('Gráfico 2D-3')
    ax4.set_xlabel('Y')
    ax4.set_ylabel('Z')

    plt.tight_layout()
    plt.show()
        
# Andrés Felipe Riaño Quintanilla 1083928808
# Santiago Julio Dávila 1000413445

import numpy as np
import matplotlib.pyplot as plt

class RKG:
    nvar = 1
    def __init__(self,orden,cis,aijs,bis,paso,func,inicial,xfinal):
        '''
        RKG resuelve una EDO de primer orden usando el método de Runge-Kutta generalizado:
        y_(n+1) = y_n + h*Σb_i*K_i

        donde K_i = f(x_n + c_i*h,y_n + (Σa_(ij)*K_j)*h)

        INPUT:
        orden: orden del método RK (int)
        cis: lista de las constantes c_i, la longitud de la lista debe ser igual al 
             orden y el primer elemento debe ser 0 (list).
        aijs: lista de listas de las constantes a_(ij), empezando con i=2,
              cada elemento de la lista debe ser una lista con i-1 entradas.
        bis: lista de las constantes b_i, la longitud de la lista debe ser igual al
             orden (list).
        paso: paso deseado para implementar el método RK (float).
        func: función f(x,y) que representa la EDO.
        inicial: condiciones iniciales (x_0,y_0) (list).
        xfinal: valor final de x (float).

        WARNING: numpy y matplotlib son requeridos para instanciar esta clase.

        ATRIBUTOS:
        orden: orden del método RK
        cs: constantes c_i
        As: constantes a_(ij)
        bs: constantes b_i
        paso: paso del método RK
        funcion: función que representa la derivada
        x0: condición inicial de x
        y0: condición inicial de y
        nvar: número de variables dependientes (1)

        MÉTODOS:
        ks, soluciones, convergencia
        '''

        if cis[0] != 0:
            raise Exception(f'c_1 debe ser 0, usted ingresó {cis[0]}')
        
        self.orden = orden
        self.cs = np.array(cis)

        # construye una matriz con los coeficientes a_(ij)
        A = np.zeros((self.orden,self.orden))
        for i in range(self.orden-1):
            A[i+1,0:len(aijs[i])] = aijs[i]
        
        self.As = A
        self.bs = bis
        self.paso = paso
        self.funcion = func
        self.x0 = inicial[0]
        self.y0 = inicial[1]
        self.xf = xfinal

    def ks(self,xn,yn,**kwargs):
        '''
        Calcula las K_i del método RK de la instancia.
        
        INPUT:
        xn: valor de x para el que se calcularán las K_i
        yn: valor de y para el que se calcularán las K_i

        OUTPUT:
        Ks: vector con las K_i
        '''
        Ks = np.zeros((self.orden,self.nvar))
        for i in range(self.orden):
            Ks[i] = self.funcion(xn+self.cs[i]*self.paso,yn+(self.As@Ks)[i]*self.paso,**kwargs)
        return Ks
    
    def soluciones(self,vars,graph=True,sols=False,exact=None):
        '''
        Resuelve la ecuación diferencial utilizando el método RK especificado.

        INPUT:
        vars: lista de strings que corresponden a las variables independientes,
              debe ser de longitud igual a nvar. Se usan para las etiquetas de la gráfica.
        graph: booleano que determina si el método entrega la gráfica de la solución, True por defecto.
        sols: booleano que determmina si el método entrega la solución numérica, False por defecto.
        exact: permite al usuario ingresar la solución exacta para graficar y obtener los valores numéricos, None por defecto.

        OUTPUT:
        Gráfica de la solución numérica y la solución exacta, si la hay; y/o diccionario con la solución
        numérica.
        '''
        xs = np.arange(self.x0,self.xf,self.paso)
        if self.nvar != 1:
            ys = np.zeros((len(xs),self.nvar))
        else:
            ys = np.zeros_like(xs)
        ys[0] = self.y0
        for i,x in enumerate(xs[:-1]):
            ys[i+1] = ys[i] + self.paso*(self.bs@self.ks(x,ys[i]))

        if graph:
            plt.plot(xs,ys,label=vars)
            if exact:
                plt.plot(xs,exact(xs),'k:',label='Sol. exacta')
            plt.title('Solución de la EDO')
            plt.xlabel('X')
            plt.ylabel('Y')
            plt.grid()
            plt.legend()
            plt.savefig('ex1.pdf')
            plt.show()
        
        if sols:
            if exact:
                slns = {'xs':xs, 'ys_RK':ys, 'ys_ex':exact(xs)}
            else:
                slns = {'xs':xs, 'ys_RK':ys}
            return slns
        
    def convergencia(self,f_ex,hmin=1e-5,hmax=0.1,niter=20):
        '''
        Evalúa la convergencia de la solución numérica dada la solución analítica.

        INPUT:
        f_ex: solución exacta de la EDO (función).
        h_min: paso mínimo, 1e-5 por defecto (float).
        h_max: paso máximo, 0.1 por defecto (float).
        niter: número de iteraciones entre el mínimo y el máximo, 20 por defecto (int).

        OUTPUT:
        Gráfica en escala logarítmica del error de la solución numérica.
        '''

        # Por facilidad en la visualización, se usa escala loglog
        nmin = np.log10(hmin)
        nmax = np.log10(hmax)
        hs = np.logspace(nmin,nmax,niter)
        deltas = np.zeros_like(hs)

        for i,h in enumerate(hs):
            self.paso = h
            slns = self.soluciones(None,graph=False,sols=True,exact=f_ex)
            Delta = np.mean(np.abs(slns['ys_RK']-slns['ys_ex']))
            deltas[i] = Delta

        print('ADVERTENCIA: el método puede tardarse un tiempo en entregar la gráfica.')

        plt.loglog(hs,deltas,'r')
        plt.title(f'Convergencia del método RK{self.orden}')
        plt.xlabel('h')
        plt.ylabel('Δy')
        plt.grid()
        plt.savefig('ex1_conv.pdf')
        plt.show()


class RKG_A(RKG):
    def __init__(self, orden, cis, aijs, bis, paso, func, inicial, xfinal, nvar):
        '''
        Subclase de RKG que permite modificar nvar para resolver sistemas de EDOs acoplados.
        Hereda los métodos y atributos de RKG.

        INPUT:
        orden: orden del método RK (int)
        cis: lista de las constantes c_i, la longitud de la lista debe ser igual al 
             orden y el primer elemento debe ser 0 (list).
        aijs: lista de listas de las constantes a_(ij), empezando con i=2,
              cada elemento de la lista debe ser una lista con i-1 entradas.
        bis: lista de las constantes b_i, la longitud de la lista debe ser igual al
             orden (list).
        paso: paso deseado para implementar el método RK (float).
        func: función f(x,y) que representa la EDO.
        inicial: condiciones iniciales (x_0,y_0) (list).
        xfinal: valor final de x (float).
        nvar: número de variables independientes a resolver (int).

        WARNING: numpy y matplotlib son requeridos para instanciar esta clase.

        ATRIBUTOS:
        orden: orden del método RK
        cs: constantes c_i
        As: constantes a_(ij)
        bs: constantes b_i
        paso: paso del método RK
        funcion: función que representa la derivada
        x0: condición inicial de x
        y0: condición inicial de y
        nvar: número de variables dependientes

        MÉTODOS:
        ks, soluciones, convergencia, soluciones_3D
        '''
        super(RKG_A,self).__init__(orden, cis, aijs, bis, paso, func, inicial, xfinal)
        self.nvar = nvar

    def soluciones_3D(self,vars,nom,tit=None):
        '''
        Grafica las soluciones de U_1, U_2, U_3 respecto a x, y grafica la solución en el espacio
        tridimensional.

        INPUT:
        vars: lista de strings que corresponden a las variables independientes,
              debe ser de longitud igual a nvar. Se usan para las etiquetas de la gráfica.
        nom: nombre con el que se guardará la gráfica en formato .pdf. (str)
        tit: Permite al usuario agregar un título a la gráfica, None por defecto. (str)

        OUTPUT:
        Gráficos de las tres funciones respecto a x, y gráfico 3D de la solución.

        ADVERTENCIAS:
        - Este método solo funciona si nvar == 3.
        - El método puede tardar un tiempo considerable en entregar las gráficas.
        '''
        print('ADVERTENCIA: El método puede tardar un tiempo considerable en entregar las gráficas.')

        slns = self.soluciones(vars,graph=False,sols=True,)
        fig = plt.figure(figsize=(10,10))
        fig.suptitle(tit)

        for i in range(self.nvar):
            ax = fig.add_subplot(2,2,i+1)
            ax.plot(slns['xs'],slns['ys_RK'][:,i])
            ax.set(title=f'{vars[i]} vs t',xlabel='t',ylabel=vars[i])
            ax.grid()
        
        
        ax = fig.add_subplot(2,2,4,projection='3d')
        ax.plot(*slns['ys_RK'].T,lw=0.25,label=f'r_0={self.y0}')
        ax.set(title=f'Espacio {vars[0]}{vars[1]}{vars[2]}',xlabel=vars[0],ylabel=vars[1],zlabel=vars[2])
        ax.legend()
        plt.savefig(nom+'.pdf')
        plt.subplots_adjust(wspace=0.2,
                    hspace=0.3)
        plt.show()
# Parcial - Física Computacional 1

Parcial de Física Computacional 1, Universidad de Antioquia.

David Garcia Gomez (1000439477)
Tomas Sosa Giraldo (1000293341)

## Método de Runge-Kutta

El método generalizado de Runge-Kutta es una familia de métodos iterativos para resolver ecuaciones diferenciales ordinarias (EDOs). Estos métodos se caracterizan por su orden de precisión y están diseñados para aproximar la solución de una EDO de la forma:

$$
\frac{dy}{dt} = f(t, y)
$$

con una condición inicial $y(t_0) = y_0$. Los métodos involucran tomar promedios ponderados de varias estimaciones de la pendiente (derivada) de la curva solución, calculadas en diferentes puntos dentro de cada paso. La forma general de un método de Runge-Kutta para un paso de $t_n$ a $t_{n+1}$ se puede escribir de la siguiente manera:

1. Calcular $k$ pendientes intermedias, donde cada pendiente $k_i$ se define por

$$
k_i = f\left(t_n + c_i h, y_n + h \sum_{j=1}^{i-1} a_{ij} k_j\right), \quad i=1, \ldots, k.
$$

Aquí, $h$ es el tamaño del paso, $a_{ij}$, $c_i$ y $b_i$ son coeficientes que definen el método particular de Runge-Kutta, y $k$ es el número de etapas del método.

2. Actualizar la solución usando una suma ponderada de estas pendientes:

$$
y_{n+1} = y_n + h \sum_{i=1}^{k} b_i k_i.
$$

La elección de los coeficientes $a_{ij}$, $b_i$ y $c_i$ determina el orden de precisión y las propiedades de estabilidad del método. Para que un método de Runge-Kutta sea de orden $p$, debe integrar exactamente polinomios de hasta grado $p$. El método más simple de Runge-Kutta es el método de Euler, que es de primer orden con $k=1$. El método de Runge-Kutta de cuarto orden (RK4) tiene $k=4$ y valores específicos para $a_{ij}$, $b_i$ y $c_i$ que lo hacen de cuarto orden en precisión.

Para especificar completamente un método de Runge-Kutta, a menudo se utiliza un tableau de Butcher, una representación matricial que lista de manera compacta sus coeficientes:

$$
\begin{array}{c|cccc}
c_1 & a_{11} & a_{12} & \cdots & a_{1k} \\
c_2 & a_{21} & a_{22} & \cdots & a_{2k} \\
\vdots & \vdots & \vdots & \ddots & \vdots \\
c_k & a_{k1} & a_{k2} & \cdots & a_{kk} \\
\hline
& b_1 & b_2 & \cdots & b_k \\
\end{array}
$$

La expansión en serie de Taylor muestra que el método de Runge–Kutta es consistente si y solo si la suma de los coeficientes $b_i$ satisface la siguiente condición:

$$
\sum_{i=1}^{s} b_i = 1.
$$

Esta condición es necesaria para la consistencia, que es el comportamiento correcto del método a medida que el tamaño del paso $h$ tiende a cero.

Una condición popular para los coeficientes $a_{ij}$ es que para cada etapa $i$, la suma de los coeficientes $a_{ij}$ de esa etapa sea igual a $c_i$:

$$
\sum_{j=1}^{i-1} a_{ij} = c_i \quad \text{para} \quad i = 2, \ldots, s.
$$

Sin embargo, esta condición por sí sola no es ni suficiente ni necesaria para la consistencia del método, lo que implica que cumplir con esta condición no garantiza automáticamente la precisión deseada del método.

En definitiva, los métodos de Runge-Kutta son poderosos porque ofrecen un buen compromiso entre precisión y eficiencia computacional para muchos problemas prácticos. Su marco general permite el desarrollo de métodos adaptados a necesidades específicas, como métodos con tamaños de paso adaptativos o métodos optimizados para ecuaciones diferenciales rígidas.

### Runge-Kutta de cuarto orden (RK4)

El método de Runge-Kutta de cuarto orden (RK4) es uno de los algoritmos numéricos más utilizados para resolver ecuaciones diferenciales ordinarias debido a su buena precisión y estabilidad relativa. A continuación, se describe cómo el método RK4 aproxima la solución de una ecuación diferencial ordinaria dada.

Dada una ecuación diferencial de la forma $\frac{dy}{dt} = f(t, y)$ con una condición inicial $y(t_0) = y_0$, el método RK4 calcula aproximaciones sucesivas de $y$ en puntos discretos de tiempo, avanzando en pasos de tamaño $h$. El método utiliza cuatro "pendientes" intermedias para calcular la aproximación en el siguiente punto. Estas pendientes se calculan como sigue:

$$
\begin{aligned}
k_1 &= f(t_n, y_n), \\
k_2 &= f(t_n + \frac{h}{2}, y_n + \frac{h}{2}k_1), \\
k_3 &= f(t_n + \frac{h}{2}, y_n + \frac{h}{2}k_2), \\
k_4 &= f(t_n + h, y_n + hk_3),
\end{aligned}
$$

donde $t_n$ es el tiempo actual, $y_n$ es la aproximación actual de $y$, y $h$ es el paso de tiempo. Luego, la aproximación para $y$ en el siguiente paso de tiempo $t_{n+1}$ es calculada por la fórmula:

$$
y_{n+1} = y_n + \frac{h}{6}(k_1 + 2k_2 + 2k_3 + k_4).
$$

Para su implementación en código, el método RK4 requiere una función que pueda calcular el lado derecho de la ecuación diferencial, $f(t, y)$, y luego iterar sobre el rango de tiempo deseado, aplicando las fórmulas anteriores.

El tableau de Butcher correspondiente al método RK4 es:

$$
\begin{array}{c|cccc}
0 &  \\
\frac{1}{2} & \frac{1}{2} &  \\
\frac{1}{2} & 0 & \frac{1}{2} &  \\
1 & 0 & 0 & 1 \\
\hline
& \frac{1}{6} & \frac{1}{3} & \frac{1}{3} & \frac{1}{6}
\end{array}
$$

Este tableau encapsula la estrategia de evaluación y ponderación de las pendientes utilizada por el método RK4.

## Ecuaciones de Lorenz

Las ecuaciones de Lorenz son un sistema de ecuaciones diferenciales ordinarias tridimensionales que se han estudiado extensamente en teoría del caos debido a su estructura que puede producir comportamientos dinámicos muy complejos a partir de un conjunto de ecuaciones relativamente sencillo. Estas ecuaciones fueron introducidas por Edward Lorenz en 1963 durante sus estudios sobre convección atmosférica.

El sistema de ecuaciones de Lorenz se describe con las siguientes fórmulas:

$$
\begin{aligned}
\frac{dx}{dt} &= \sigma(y - x), \\
\frac{dy}{dt} &= x(\rho - z) - y, \\
\frac{dz}{dt} &= xy - \beta z.
\end{aligned}
$$

Donde:
- $x, y, z$ representan el estado del sistema en un tiempo $t$.
- $\sigma$ es el número de Prandtl, que suele ser positivo y representa la tasa de difusión viscosa.
- $\rho$ es el número de Rayleigh, que representa la diferencia de temperatura en la convección.
- $\beta$ es un parámetro que está relacionado con las proporciones físicas del sistema de convección.

Estos parámetros pueden ajustarse para cambiar el comportamiento del sistema, y hay ciertos valores de estos parámetros para los cuales el sistema exhibe un atractor extraño, un tipo de objeto fractal que puede ser visualizado en el espacio de fases y es un indicador de comportamiento caótico.

Las ecuaciones de Lorenz son un ejemplo clásico de cómo el comportamiento no lineal en los sistemas dinámicos puede conducir a un comportamiento que es muy sensible a las condiciones iniciales, un fenómeno a menudo descrito como el "efecto mariposa".

Para resolver numéricamente las ecuaciones de Lorenz, se puede utilizar el método de Runge-Kutta descrito anteriormente, eligiendo un paso de tiempo adecuado y aplicando las fórmulas iterativamente para cada una de las ecuaciones en el sistema.

## Notas Importantes sobre las soluciones

Se adjunta el requeriments.txt para instalar las librerías necesarias para correr todos los código.

### Solución 1

Las imagenes llamadas "Error.png" y "x-y_graph.png" corresponden al primer punto.

Las imágenes llamadas "lorentz_i" y "Graphs_i" corresponden a las gráficas 3D y a las gráficas de x vs y, x vs z y y vs z para cada i que corresponden a los valores iniciales de los puntos 1, 2, 3 y 4.


COMO LAS CONDICIONES INICIALES DE 1 Y 3 SON X=0, Y=0, Z=0. LA FUNCIÓN SIEMPRE DA 0, POR ESO LAS IMÁGENES ESTÁN EN BLANCO, NO ES UN ERROR
PERO COMO SON LOS PROBLEMAS PROPUESTOS, SE INCLUYEN EN EL TRABAJO.

En el caso del punto 2 y 4, sí se puede evidenciar el comportamiento de el atractor de lorentz.

### Solución 2

Los códigos del Runge-Kutta generalizado y su ejecución son RKG.py y main_RKG.py respectivamente. En la carpeta Plots_RKG se encuentran las gráficas generadas por el código main_RKG.py y por tanto los entregables requeridos.

Los códigos referentes a la solución del sistema de ecuaciones de Lorenz son Lorenz.py y main_Lorenz.py. En la carpeta Plots_Lorenz se encuentran las gráficas generadas por el código main_Lorenz.py, donde First_Case representa el caso donde se toman $\sigma = 10$, $\rho = 28$ y $\beta = \frac{8}{3}$, y Second_Case representa el caso donde $\sigma = 16$, $\rho = 45$ y $\beta = 4$.
from punto2 import RKGA
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D

if __name__ =='__main__':
    # Definimos la función derivada:
    def derivada_lorenz(x, estado, sigma, rho, beta):
        dx, dy, dz = estado
        return np.array([sigma*(dy-dx), dx*(rho-dz)-dy, dx*dy -beta*dz])

    condiciones_iniciales = np.array([1.0, 1.0, 1.0]) # vector de condiciones iniciales para las variables x, y, z
    t0, tf, h = 0, 100, 0.01
    a = [[0, 0, 0, 0], [0.5, 0, 0, 0], [0, 0.5, 0, 0], [0, 0, 1, 0]]    # Constantes de Butcher para RK4
    b = [1/6, 1/3, 1/3, 1/6]                                            # Constantes de Butcher para RK4
    c = [0, 0.5, 0.5, 1]                                                # Constantes de Butcher para RK4
    constantes_butcher = [a, b, c]

    sigmas = [10, 10, 16, 16]
    rhos = [28, 28, 45, 45]
    betas = [8/3, 8/3, 4, 4]
    x0 = [0.0, 0.0, 0.0, 0.0]
    y0 = [0.0, 0.01, 0.0, 0.001]
    z0 = [0.0, 0.0, 0.0, 0.0]

    #Generación de gráficas:
    
    for sigma, rho, beta,x0,y0,z0 in zip(sigmas, rhos, betas,x0,y0,z0):
        lorenz_rkga = RKGA(lambda x, estado: derivada_lorenz(x, estado, sigma, rho, beta), t0, [x0,y0,z0], tf, h, constantes_butcher)
        r, soluciones = lorenz_rkga.soluciones()

        x = soluciones[:,0]
        y = soluciones[:,1]
        z = soluciones[:,2]
        # Grafica 3D
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        # Trazar la solución
        ax.plot(x, y, z, lw=0.5)

        ax.set_title(f"Atractor de Lorenz (sigma={sigma}, rho={rho}, beta={beta}),(x0={x0}, y0={y0}, z0={z0})")
        ax.set_xlabel("Eje X")
        ax.set_ylabel("Eje Y")
        ax.set_zlabel("Eje Z")

        plt.savefig('./Punto_3/Imagenes/fig_3d_'+str(x0)+'_'+str(y0)+'_'+str(z0)+'_'+str(sigma)+'_'+str(rho)+'_'+str(beta)+'.png')
        #plt.show()
        
        # Grafica 2D x vs y
        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111)
        # Trazar la solución
        ax1.plot(x, y)

        ax1.set_title(f"Atractor de Lorenz (sigma={sigma}, rho={rho}, beta={beta}),(x0={x0}, y0={y0}, z0={z0})")
        ax1.set_xlabel("Eje X")
        ax1.set_ylabel("Eje Y")
        
        plt.savefig('./Punto_3/Imagenes/fig_2d_'+'x_vs_y_'+str(x0)+'_'+str(y0)+'_'+str(z0)+'_'+str(sigma)+'_'+str(rho)+'_'+str(beta)+'.png')
        
        # Grafica 2D x vs z
        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111)
        # Trazar la solución
        ax1.plot(x, z)

        ax1.set_title(f"Atractor de Lorenz (sigma={sigma}, rho={rho}, beta={beta}),(x0={x0}, y0={y0}, z0={z0})")
        ax1.set_xlabel("X")
        ax1.set_ylabel("Z")
        
        plt.savefig('./Punto_3/Imagenes/fig_2d_'+'x_vs_z_'+str(x0)+'_'+str(y0)+'_'+str(z0)+'_'+str(sigma)+'_'+str(rho)+'_'+str(beta)+'.png')
        # Nombre de la figura

        # Grafica 2D y vs z
        fig2 = plt.figure()
        ax2 = fig2.add_subplot(111)
        # Trazar la solución
        ax2.plot(y, z)

        ax2.set_title(f"Atractor de Lorenz (sigma={sigma}, rho={rho}, beta={beta}),(x0={x0}, y0={y0}, z0={z0})")
        ax2.set_xlabel("X")
        ax2.set_ylabel("Z")
        
        plt.savefig('./Punto_3/Imagenes/fig_2d_'+'y_vs_z_'+str(x0)+'_'+str(y0)+'_'+str(z0)+'_'+str(sigma)+'_'+str(rho)+'_'+str(beta)+'.png')
        # Nombre de la figura
        

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

class parabolic:   
    def __init__ (self, angle: float, initial_velocity: float, initial_heigth: float, initial_x:float) -> None:
        '''
        Given some initial conditions for the parabolic movement, we define the properties of the object.
        
        args:
        - angle: is a float, ninitial angle in degrees 
        - initial_velocity: is the modulous of the initial velocity of movement.
        - initial height: float, intial height of movement
        - inicial_x: is the position on x from which the movement starts.
        '''
        self.Theta = angle * np.pi /180
        self.v0 = initial_velocity
        self.y0 = initial_heigth
        self.x0 = initial_x
    
    
    def trajectory(self):
        '''
        This method, calculates the trayectory of an object given some initial conditions defined in the constructor supposing 
        constant acceleration.
        
        returns:
        - Velocity at all time in x and y directions
        - Total time of movement
        - position x and y
        - plots the position.
        -Saves the information in a csv file.
        '''
        g:float = 9.8 
        total_time:float = ( -self.v0 * np.sin(self.Theta) - ( (self.v0 * np.sin(self.Theta))**2 + 2 * g * self.y0)**(0.5) ) / ( -g )
        t:float = np.arange(0,total_time,0.001)
        
        #calculating position.
        x:np.ndarray = self.x0 + self.v0 * np.cos(self.Theta) * t
        y:np.ndarray = self.y0 + self.v0 * np.sin(self.Theta) * t - 0.5 * g * t*t
        
        #ecuations for velocity
        velocity_x:np.ndarray = self.v0 * np.cos(self.Theta)
        velocity_y:np.ndarray = self.v0 * np.sin(self.Theta) - g * t
        
        #Ploting the results
        fig = plt.figure(figsize=[10,7])
        plt.plot(x,y,'--k',label = 'Trajectory')
        plt.plot(x[-1],y[-1],'co' ,label='Final position')
        
        plt.ylabel(f'$y(m)$')
        plt.xlabel(f'$x(m)$\n\n cond: v_0= {self.v0}m/s,  x_0={self.x0}m,  y_0= {self.y0}m,  theta= {np.round(self.Theta,3)}rad,  total_time={np.round(total_time,4)}s')
        
        plt.title('Position of particle for parabolic movement.\n')
        
        
        plt.legend(loc = 'best')
        plt.grid(True)
        plt.savefig('trajectory.png', dpi = 250, format = 'png' )
        
        #saving the data in a csv file.
        data = pd.DataFrame(
            {'x (m)': x, 
             'y (m)': y,
             'vx (m/s)': velocity_x,
             'vy (m/s)': velocity_y,
             'total_time (s)': total_time
            }).reset_index()
        data.to_csv('data.csv' , sep =',', index= False, encoding='utf-8')
        
        
        
        
        
        


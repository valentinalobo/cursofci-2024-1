from tiroparabolico import tiroParabolico, tiroParabolico2
import numpy as np

if __name__=="__main__":
    print("incialización para graficar el movimiento 2D del tirpo parabolico")
    
    theta=(np.pi)/3
    v0=10 #m/S
    x0=0
    h0=20
    g=9.8 #m/s^2
    acelx=-9.8
    #sol=tiroParabolico(theta,v0,x0,h0,g)
    #sol.grafica()

    sol2=tiroParabolico2(theta,v0,x0,h0,g,acelx)
    sol2.grafica()
    print(sol2.tfly())